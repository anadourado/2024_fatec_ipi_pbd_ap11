#Exercício 11 

#1.1 

CREATE TABLE log_operations (
    log_id SERIAL PRIMARY KEY,
    operation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    operation_name VARCHAR(100)
);

CREATE TABLE order (
    customer_id INT PRIMARY KEY,
    order_date DATE,
    order_details VARCHAR(100)
);

CREATE OR REPLACE PROCEDURE add_order(customer_id INT, order_details TEXT) 
LANGUAGE plpgsql AS $$
BEGIN
    -- Inserir o pedido na tabela de pedidos 
    INSERT INTO orders (customer_id, details) VALUES (customer_id, order_details);
    
    -- Registrar a operação no log
    INSERT INTO log_operations (operation_name) VALUES ('add_order');
END;
$$;

#1.2

CREATE OR REPLACE PROCEDURE get_total_orders(customer_id INT)
LANGUAGE plpgsql AS $$
DECLARE
    total_orders INT;
BEGIN
    SELECT COUNT(*) INTO total_orders FROM orders WHERE customer_id = customer_id;
    RAISE NOTICE 'Total orders for customer %: %', customer_id, total_orders;
    
    -- Registrar a operação no log
    INSERT INTO log_operations (operation_name) VALUES ('get_total_orders');
END;
$$;

#1.3

CREATE OR REPLACE PROCEDURE get_total_orders_out(customer_id INT, OUT total_orders INT)
LANGUAGE plpgsql AS $$
BEGIN
    SELECT COUNT(*) INTO total_orders FROM orders WHERE customer_id = customer_id;
    
    -- Registrar a operação no log
    INSERT INTO log_operations (operation_name) VALUES ('get_total_orders_out');
END;
$$;

#1.4
CREATE OR REPLACE PROCEDURE get_total_orders_inout(INOUT customer_id INT)
LANGUAGE plpgsql AS $$
BEGIN
    
    -- Neste caso, customer_id na entrada é o código do cliente
    -- Na saída, será o número total de pedidos
    SELECT COUNT(*) INTO customer_id FROM orders WHERE customer_id = customer_id;
    
    INSERT INTO log_operations (operation_name) VALUES ('get_total_orders_inout');
END;
$$;

DO $$
DECLARE
    total_orders INT;
BEGIN
    CALL get_total_orders_out(1, total_orders);  -- Supondo que 1 é um código de cliente existente
    RAISE NOTICE 'Total orders: %', total_orders;
END;
$$;

#1.5

CREATE OR REPLACE PROCEDURE add_clients(OUT result_text TEXT, VARIADIC client_names TEXT[])
LANGUAGE plpgsql AS $$
DECLARE
    client_name TEXT;
    names_list TEXT := '';
BEGIN
    FOREACH client_name IN ARRAY client_names
    LOOP
        INSERT INTO clients (name) VALUES (client_name);

        IF names_list = '' THEN
            names_list := client_name;
        ELSE
            names_list := names_list || ', ' || client_name;
        END IF;
    END LOOP;
    
    result_text := 'Os clientes: ' || names_list || ' foram cadastrados';

    INSERT INTO log_operations (operation_name) VALUES ('add_clients');
END;
$$;

#1.6
 
DO $$
BEGIN
    CALL get_total_orders(1);  -- Supondo que 1 é um código de cliente existente
END;
$$;

DO $$
DECLARE
    total_orders INT;
BEGIN
    CALL get_total_orders_out(1, total_orders);  -- Supondo que 1 é um código de cliente existente
    RAISE NOTICE 'Total orders: %', total_orders;
END;
$$;

DO $$
DECLARE
    customer_id INT := 1;  -- Supondo que 1 é um código de cliente existente
BEGIN
    CALL get_total_orders_inout(customer_id);
    RAISE NOTICE 'Total orders for customer 1: %', customer_id;
END;
$$;

DO $$
DECLARE
    result_text TEXT;
BEGIN
    CALL add_clients(result_text, 'Pedro', 'Ana', 'João');
    RAISE NOTICE '%', result_text;
END;
$$;


